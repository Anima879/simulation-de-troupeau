"""
Programme simulant un troupeaux d'individus suivant 3 règles simples:
    - L'alignement : Les individus cherchent à s'aligner avec ses camarades.
    - La cohésion : Les individus cherchent à se rapprocher de ses camarades.
    - La séparation : Les individus cherchent à éviter les collisions avec ses camarades.

Pour plus de détails, voir le site de Craig Reynold : https://www.red3d.com/cwr/boids/

Un individu est nommé "gueu".
Un troupeau d'invidus est nommé "gueugueu".
"""

from sketch import Canvas
from Gueu.Gueu import Gueu
import tkinter as tk
from random import randint, uniform, choice
import numpy as np

__VERSION__ = "1.0"
__AUTHOR__ = "Eloi Mahé"


def generate_gueu(size):
    """
        Créé la population initiale de "gueu" (individu).
    :param size: {int} nombre d'invidus.
    :return gueugueu: {list} Liste des individus.
    """
    gueugeu = []
    colors = ['white', 'red', 'yellow', 'green', 'purple', 'blue']

    for i in range(size):
        pos = np.array([randint(0, 1000), randint(0, 600)], dtype=np.float32)
        heading = uniform(0, 2 * np.pi)
        g = Gueu(pos, heading)
        g.color = choice(colors)
        gueugeu.append(g)

    return gueugeu


def main():
    # Création de la fenêtre graphique
    root = tk.Tk()
    root.title("Flock")
    canvas = Canvas(root)

    # Référence de la toile graphique donnée à l'objet Gueu.
    Gueu.canvas = canvas

    pop_size = 70
    gueugeu = generate_gueu(pop_size)

    # Set scales
    canvas.scale_align.set(100)
    canvas.scale_cohesion.set(50)
    canvas.scale_separation.set(55)
    canvas.scale_perc.set(50)

    while True:
        try:
            # Mise à jour des valeurs selon les curseurs.
            Gueu.max_force_cohesion = canvas.scale_cohesion.get()
            Gueu.max_force_separation = canvas.scale_separation.get()
            Gueu.max_force_align = canvas.scale_align.get()
            Gueu.watch_radius = canvas.scale_perc.get()

            if canvas.reset_flag:
                gueugeu = generate_gueu(pop_size)
                canvas.reset_flag = False

            for g in gueugeu:
                g.update(gueugeu)
            canvas.update()
        except tk.TclError:
            break


if __name__ == "__main__": main()
