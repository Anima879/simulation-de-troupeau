import numpy as np
import random


class Gueu:
    """
        Classe modélisant un individu.
    """

    canvas = None
    max_force_align = 100
    max_force_cohesion = 20
    max_force_separation = 22
    force_repulsion = 500
    watch_radius = 50

    def __init__(self, pos, heading):
        """
            Constructeur de la classe.
        :param pos: {np.array} or {list: int} Vecteur position.
        :param heading: {int} direction de l'individu. Compris entre 0 et 2*PI
        """

        if Gueu.canvas is None:
            raise ValueError("Cannot create an instance of Gueu without a defined canvas.")

        self.position = np.array(pos)
        self.speed = random.choice([30, 35, 40, 45, 50])

        self.velocity = np.array([np.cos(heading), np.sin(heading)]) * self.speed
        self.acceleration = np.zeros(2)
        self.has_flock_instinct = True

        self.color = 'white'
        self.radius = 2.5

        self.g_obj = None
        self.g_perc = None
        self.g_arrow = None

    def __del__(self):
        Gueu.canvas.canvas.delete(self.g_perc)
        Gueu.canvas.canvas.delete(self.g_obj)
        Gueu.canvas.canvas.delete(self.g_arrow)

    def show(self):
        Gueu.canvas.canvas.delete(self.g_obj, self.g_arrow)
        self.g_obj = Gueu.canvas.canvas.create_oval(self.position[0] - self.radius, self.position[1] - self.radius,
                                                    self.position[0] + self.radius,
                                                    self.position[1] + self.radius, fill=self.color)

        vec = self.velocity / self.norm(self.velocity)
        self.g_arrow = Gueu.canvas.canvas.create_line(self.position[0], self.position[1], self.position[0] + vec[0],
                                                      self.position[1] + vec[1], fill=self.color, arrow='last',
                                                      width=0.3)

    def edges(self):
        """
            Transforme le plan en Tor.
            Lorsqu'un individu arrive au bord, il est téléporté de l'autre côté du plan.
        """
        if self.position[0] > 1000:
            self.position[0] = 0
            return True
        elif self.position[0] < 0:
            self.position[0] = 1000
            return True

        if self.position[1] > 600:
            self.position[1] = 0
            return True
        elif self.position[1] < 0:
            self.position[1] = 600
            return True

        return False

    def edges_repulsion(self):
        """
            Applique une force de répulsion aux bords de la toile.
        """
        repulsion = np.zeros(2)
        if self.position[0] > Gueu.canvas.dimension[0] - 50:
            d = Gueu.canvas.dimension[0] - self.position[0]
            repulsion += np.array([- Gueu.force_repulsion / (d ** 2), 0])
        elif self.position[0] < 50:
            repulsion += np.array([Gueu.force_repulsion / (self.position[0] ** 2), 0])

        if self.position[1] > Gueu.canvas.dimension[1] - 50:
            d = Gueu.canvas.dimension[1] - self.position[1]
            repulsion += np.array([0, - Gueu.force_repulsion / (d ** 2)])
        elif self.position[1] < 50:
            repulsion += np.array([0, Gueu.force_repulsion / (self.position[1] ** 2)])

        return repulsion

    def align(self, gueugueu):
        total = 0
        steering = np.zeros(2)
        for g in gueugueu:
            d = self.norm(self.position - g.position)
            if d < Gueu.watch_radius and g != self:
                total += 1
                steering += g.velocity

        if total > 0:
            steering /= total
            steering = (steering / self.norm(steering)) * self.speed
            steering -= self.velocity

            if self.norm(steering) > Gueu.max_force_align:
                steering = (steering / self.norm(steering)) * Gueu.max_force_align

        return steering

    def separation(self, gueugueu):
        total = 0
        steering = np.zeros(2)
        for g in gueugueu:
            d = self.norm(self.position - g.position)
            if d < Gueu.watch_radius and g != self:
                total += 1
                diff = self.position - g.position
                diff /= d * d
                steering += diff

        if total > 0:
            steering /= total
            steering = (steering / self.norm(steering)) * self.speed
            steering -= self.velocity

            if self.norm(steering) > Gueu.max_force_separation:
                steering = (steering / self.norm(steering)) * Gueu.max_force_separation

        return steering

    def cohesion(self, gueugueu):
        total = 0
        steering = np.zeros(2)
        for g in gueugueu:
            d = self.norm(self.position - g.position)
            if d < Gueu.watch_radius and g != self:
                total += 1
                steering += g.position

        if total > 0:
            steering /= total
            steering -= self.position
            steering = (steering / self.norm(steering)) * self.speed
            steering -= self.velocity

            if self.norm(steering) > Gueu.max_force_cohesion:
                steering = (steering / self.norm(steering)) * Gueu.max_force_cohesion

        return steering

    def show_perception(self, show=False):
        if show:
            Gueu.canvas.canvas.delete(self.g_perc)
            self.g_perc = Gueu.canvas.canvas.create_oval(self.position[0] - Gueu.watch_radius,
                                                         self.position[1] - Gueu.watch_radius,
                                                         self.position[0] + Gueu.watch_radius,
                                                         self.position[1] + Gueu.watch_radius, outline='red',
                                                         fill=None)

    def flock(self, gueugueu):
        """
            Combine les trois critères en une boucle.
        """
        total = 0
        steering = np.zeros(2)
        separation = np.zeros(2)
        cohesion = np.zeros(2)
        for g in gueugueu:
            d = self.norm(self.position - g.position)
            if d < Gueu.watch_radius and g != self:
                total += 1
                cohesion += g.position

                diff = self.position - g.position
                diff /= d * d
                separation += diff

                steering += g.velocity

        if total > 0:
            # Cohesion
            cohesion /= total
            cohesion -= self.position
            cohesion = (cohesion / self.norm(cohesion)) * self.speed
            cohesion -= self.velocity
            if self.norm(cohesion) > Gueu.max_force_cohesion:
                cohesion = (cohesion / self.norm(cohesion)) * Gueu.max_force_cohesion

            # Align
            steering /= total
            steering = (steering / self.norm(steering)) * self.speed
            steering -= self.velocity
            if self.norm(steering) > Gueu.max_force_align:
                steering = (steering / self.norm(steering)) * Gueu.max_force_align

            # Separation
            separation /= total
            separation = (separation / self.norm(separation)) * self.speed
            separation -= self.velocity
            if self.norm(separation) > Gueu.max_force_separation:
                separation = (separation / self.norm(separation)) * Gueu.max_force_separation

            return cohesion + steering + separation

        return 0

    def update(self, gueugeu=None):
        """
            Intégration d'Euler pour la mise-à-jour.
        """
        if self.has_flock_instinct:
            if gueugeu is None:
                raise ValueError("Need list of mates (Gueugueu) for update.")

            self.acceleration += self.flock(gueugeu)

        self.acceleration += self.edges_repulsion()
        self.velocity += self.acceleration * Gueu.canvas.animation_step
        self.velocity = self.velocity / self.norm(self.velocity) * self.speed
        self.position += self.velocity * Gueu.canvas.animation_step
        self.acceleration = 0

        # self.edges()
        self.show()

    @staticmethod
    def compute_distance(pos1, pos2):
        return ((pos1[0] - pos2[0]) ** 2 + (pos1[1] - pos2[1]) ** 2) ** 0.5

    @staticmethod
    def norm(vector):
        return (vector[0] ** 2 + vector[1] ** 2) ** 0.5
