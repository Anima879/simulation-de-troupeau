from Gueu.Gueu import Gueu
from Alyx.Alyx import Alyx
from Food import Food


class SuperGueu(Gueu):
    """
        Gueu with a brain.
        But still an idiot.
    """

    def __init__(self, pos, heading, brain):
        super().__init__(pos, heading)

        self.has_flock_instinct = False  # has to learn it.

        assert isinstance(brain, Alyx)
        self.brain = brain

        self.health = 200
        self.speed = 30
        self.radius = 3

        self.life_time = 0
        self.food_eaten = 0

        self.has_laid = False

    def is_dead(self):
        return self.health <= 0

    def decision(self):
        pass

    def lay(self):
        if self.food_eaten >= 3:
            self.has_laid = True
            self.food_eaten -= 3

    def closest_food(self, food):
        """
            Return the closest food in watch radius
        """

        closest = None
        d_min = self.watch_radius
        for f in food:
            d = self.norm(self.position - f.position)

            if d <= d_min:
                closest = f

        return closest

    def eat_food(self, f):
        pass

    def update(self, **kwargs):
        food = None
        for key, value in kwargs.items():
            if key == "food":
                assert type(value) == list
                food = value
            else:
                raise ValueError("Unknown option : {}. Must be -food.".format(key))

        closest_food = self.closest_food(food)

        super().update()

        self.lay()

        if closest_food is not None:
            d = self.norm(self.position - closest_food.position)
            if d <= (self.radius + closest_food.radius):
                self.health += closest_food.radius * 10
                self.food_eaten += self.radius
                closest_food.is_eaten = True

        self.health -= self.speed / 100
        self.life_time += 1
