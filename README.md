/FR\
# Simulation de troupeau
Petit programme en python simulant un troupeau d'individu basé sur la théorie de [Craig Reynolds](https://www.red3d.com/cwr/boids/).

Exécuter le fichier `mainFlock` pour lancer la simulation.

Les fichiers `Boris` et `QuadTree` sont vides. Ils serviront à optimiser la boucle de recherche et à ajouter des prédateurs.

# Simulation d'ecosystème
Work In Progress

Exécuter `mainEco`pour lancer la simulation.
