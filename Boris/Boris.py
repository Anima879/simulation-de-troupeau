import numpy as np


class Boris:

    canvas = None

    def __init__(self, pos, heading):
        """

        """

        if Boris.canvas is None:
            raise ValueError("Cannot create an instance of Boris without a defined canvas.")

        self.speed = 50

        self.position = np.array(pos)
        self.velocity = np.array([np.cos(heading), np.sin(heading)]) * self.speed
        self.acceleration = np.zeros(2)
