__author__ = "Eloi Mahé"
__version__ = "1.0"

import numpy as np
import random as rand


class Alyx:
    """
        Neuroevolution
    """

    def __init__(self, nb_inputs, pattern):
        """
            Constructor. Initialize weight and bias at zero.
        :param nb_inputs: {int} number of inputs of the neural network
        :param pattern: {list: int} number of neuron for each layer, last entry is the number of output. (i.e. [3, 3, 1]
                        3 neurons for first and second layer and one output.)
        """
        self.__pattern = pattern
        self.__nb_inputs = nb_inputs
        self.__layers = []
        self.__bias = []
        self.initialized_with = None

        """
            Mutation's attribute.
        """
        self.mutation_mean = 0
        self.mutation_scale = 0.1

        assert nb_inputs > 0

        previous = nb_inputs
        for a in pattern:
            layer = np.zeros((a, previous))
            bias = np.zeros((a, 1))
            previous = a
            self.__layers.append(layer)
            self.__bias.append(bias)

    def initialize_random_normal(self, mean, scale):
        """
            Initialize weight and bias with Gaussian distribution with given mean and scale.
        :param mean: {float}
        :param scale: {float}
        """
        for l in self.__layers:
            l += np.random.normal(mean, scale, l.shape)

        for b in self.__bias:
            b += np.random.normal(mean, scale, b.shape)

        self.initialized_with = (self.initialize_random_normal, mean, scale)

    def feed_forward(self, inputs):
        """
            Compute output of the neural network with given inputs.
        :param inputs: {np.array}
        :return: {np.array} Output
        """

        temp = []
        for x in inputs:
            temp.append([x])

        inputs = np.array(temp)
        assert inputs.shape[0] == self.__layers[0].shape[1]
        assert inputs.shape[1] == 1

        output = 0
        for layer, bias in zip(self.__layers, self.__bias):
            output = np.dot(layer, inputs) + bias
            inputs = output

        return output

    def default_mutation(self, density=0.5):
        """
            Basic mutation.
            Mutate neural network with given density based on normal distribution.
            Scale and mean are attributes.
        :param density:
        :return: Nothing
        """

        assert 0 < density <= 1

        for layer in self.__layers:
            mutation = np.zeros(layer.shape)
            for i in range(mutation.shape[0]):
                for j in range(mutation.shape[1]):
                    if rand.random() <= density:
                        mutation[i][j] += np.random.normal(self.mutation_mean, self.mutation_scale)

            layer += mutation

    def __str__(self):
        text = ""
        for i in range(0, len(self.__layers)):
            text += "Layer : " + str(i) + "\n"
            text += "Weight : \n"
            text += str(self.__layers[i]) + "\n"
            text += "Bias : \n"
            text += str(self.__bias[i]) + "\n"

        return text

    def __repr__(self):
        return self.__str__()

    def get_layers(self):
        return self.__layers

    def set_layers(self, value):
        print("Cannot modify layers")

    def get_nb_inputs(self):
        return self.__nb_inputs

    def set_nb_inputs(self, value):
        print("Cannot modify number of inputs.")

    def get_pattern(self):
        return self.__pattern

    def set_pattern(self, value):
        print("cannot modify pattern.")

    layers = property(get_layers, set_layers)
    nb_inputs = property(get_nb_inputs, set_layers)
    pattern = property(get_pattern, set_pattern)


def main():
    a = Alyx(2, [4, 1])
    a.initialize_random_normal(0, 1)
    print(a.feed_forward([1, 1]).shape)


if __name__ == '__main__': main()
