from random import randint
import numpy as np


class Food:
    """

    """
    canvas = None

    def __init__(self, pos):
        assert Food.canvas is not None
        self.radius = randint(1, 3)

        self.position = np.array(pos)

        self.g_obj = None
        self.color = 'white'

        self.is_eaten = False

    def __del__(self):
        self.canvas.canvas.delete(self.g_obj)

    def show(self):
        self.canvas.canvas.delete(self.g_obj)
        self.g_obj = Food.canvas.canvas.create_oval(self.position[0] - self.radius, self.position[1] - self.radius,
                                                    self.position[0] + self.radius,
                                                    self.position[1] + self.radius, fill=self.color)
