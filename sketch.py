import tkinter as tk


class Canvas(tk.Frame):

    animation_step = 0.1

    def __init__(self, root, **kwargs):
        self.dimension = (1000, 600)
        tk.Frame.__init__(self, root, width=self.dimension[0], height=self.dimension[1], **kwargs)
        self.grid()
        self.canvas = tk.Canvas(self, width=self.dimension[0], height=self.dimension[1], bg='black')
        self.canvas.grid(row=0, column=0)

        self.frame_scale = tk.LabelFrame(root, text='Paramètres')
        self.frame_scale.grid(row=0, column=1, sticky='n')

        # flags
        self.reset_flag = False

        # flock's scales
        self.scale_cohesion = tk.Scale(self.frame_scale, from_=0, to=100, resolution=1, orient=tk.HORIZONTAL, label='Cohésion')
        self.scale_cohesion.grid()
        self.scale_separation = tk.Scale(self.frame_scale, from_=0, to=100, resolution=1, orient=tk.HORIZONTAL, label="Séparation")
        self.scale_separation.grid()
        self.scale_align = tk.Scale(self.frame_scale, from_=0, to=100, resolution=1, orient=tk.HORIZONTAL, label="Alignement")
        self.scale_align.grid()
        self.scale_perc = tk.Scale(self.frame_scale, from_=10, to=100, resolution=1, orient=tk.HORIZONTAL, label="Perception")
        self.scale_perc.grid()

        # button
        self.reset_button = tk.Button(root, text=' Reset ', command=self.reset)
        self.reset_button.grid(row=0, column=1)

    def reset(self):
        self.reset_flag = True

