from Gueu.SuperGueu import SuperGueu
from sketch import Canvas
from Gueu.Gueu import Gueu
import tkinter as tk
from random import randint, uniform, choice
import numpy as np
from Food import Food
from Alyx.Alyx import Alyx
import time


def generate_gueu(size):
    """
        Créé la population initiale de "gueu" (individu).
    :param size: {int} nombre d'invidus.
    :return gueugueu: {list} Liste des individus.
    """
    gueugeu = []
    colors = ['white', 'red', 'yellow', 'green', 'purple', 'blue']

    for i in range(size):
        pos = np.array([randint(0, 1000), randint(0, 600)], dtype=np.float32)
        heading = uniform(0, 2 * np.pi)
        alyx = Alyx(5, [5, 5, 5, 3])
        g = SuperGueu(pos, heading, alyx)
        g.color = choice(colors)
        gueugeu.append(g)

    return gueugeu


def main():
    # Création de la fenêtre graphique
    root = tk.Tk()
    root.title("Flock")
    canvas = Canvas(root)

    # Référence de la toile graphique donnée à l'objet Gueu.
    Gueu.canvas = canvas
    Food.canvas = canvas

    gueugueu = generate_gueu(20)

    food = []
    for i in range(50):
        food.append(Food([randint(0, 1000), randint(0, 600)]))

    for f in food:
        f.show()

    while True:
        for g in gueugueu:
            g.update(food=food)

        canvas.update()

        for g in gueugueu:
            if g.is_dead():
                f = Food(g.position)
                f.show()
                food.append(f)
                gueugueu.remove(g)
                g.__del__()

            if g.has_laid:
                heading = uniform(0, 2 * np.pi)
                kid = SuperGueu(g.position, heading, g.brain)
                kid.color = g.color

                gueugueu.append(kid)
                g.has_laid = False

        for f in food:
            if f.is_eaten:
                food.remove(f)
                canvas.canvas.delete(f.g_obj)
            else:
                f.show()

        time.sleep(0.005)
        print(len(food))


if __name__ == '__main__': main()
